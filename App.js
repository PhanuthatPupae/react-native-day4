import { Provider } from "react-redux";
import React, { Component } from "react";
import store from "./store";
import Page1 from './Page1';
export default class App extends Component {
  render() {
    return (
      <Provider store = {store}>
        <Page1 />
      </Provider>
    );
  }
}
