import React, { Component } from 'react';
import {View,Text,TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';

class Page1 extends Component{
    render(){
        const {todos,appTodo} = this.props
        return(
            <View>
                <TouchableOpacity onPress={()=>{
                    appTodo(1);
                    }}>
                    <Text>Add number </Text>
                </TouchableOpacity>
                <Text>
                    Last :{todos[todos.length-1]}
                </Text>
            </View>
        )
    }
}


const mapStateToProps = (state)=>{
    return {
        todos:state.todos,
        completes:state.completes
    }
}
const mapDispatchToProps =(dispatch)=>{
    return {
        appTodo:(topic)=>{
            dispatch({
                type:'ADD_TODOS',
                topic:topic
            })
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Page1)
