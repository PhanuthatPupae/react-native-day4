import { createStore , combineReducers } from 'redux'
import CompletesRuducer from './CompletesReducer'
import TodosReducer from './TodosReducer'

const reducers = combineReducers({
    todos:TodosReducer,
    completes:CompletesRuducer
})



const store = createStore(reducers);
export default store;